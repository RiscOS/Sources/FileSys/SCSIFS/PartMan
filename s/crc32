; Copyright 2016 Castle Technology Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;
;
        AREA    |Asm$$Code|,CODE,READONLY

        GET     ListOpts
        GET     Macros
        GET     Proc

        EXPORT  crc32

; machine code routine to compute crc32 of a block of memory

; on entry r0-> buffer to work on
;          r1 = byte count
; on exit  r0 = crc32
; assumes entry is word aligned, and byte count is a multiple of 4
crc32   Entry
        ldr     r3, = 0xedb88320        ; polynomial seed
        mvn     r2, #0                  ; seed r2 with -1
byteloop
        ldrb    lr, [r0], #1
        eor     r2, r2, lr

        movs    r2, r2, lsr #1          ; do this 8 times in a row
        eorcs   r2, r2, r3              ; for 1 byte
        movs    r2, r2, lsr #1          ; it'll be quicker than a loop
        eorcs   r2, r2, r3              ; but 13 instructions longer
        movs    r2, r2, lsr #1
        eorcs   r2, r2, r3
        movs    r2, r2, lsr #1
        eorcs   r2, r2, r3
        movs    r2, r2, lsr #1
        eorcs   r2, r2, r3
        movs    r2, r2, lsr #1
        eorcs   r2, r2, r3
        movs    r2, r2, lsr #1
        eorcs   r2, r2, r3
        movs    r2, r2, lsr #1
        eorcs   r2, r2, r3
        subs    r1, r1, #1
        bne     byteloop
        sub     r1, r1, #1
        eor     r0, r2, r1
        EXIT

        END

